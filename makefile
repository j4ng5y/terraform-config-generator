VERSION = 0.2.0

.PHONY: all
all: test build

.PHONY: test
test:
	go test -coverprofile=cover.out -v ./...

.PHONY: build
build: build_linux build_darwin

.PHONY: build_linux
build_linux:
	CGO_ENABLE=0 GOOS=linux GOARCH=amd64 go build -a -o bin/tcg main.go
	cd bin; tar -zcvf tcg_${VERSION}_linux_amd64.tar.gz *; cd ..
	mv bin/tcg bin/tcg_linux

.PHONY: build_darwin
build_darwin:
	CGO_ENABLE=0 GOOS=darwin GOARCH=amd64 go build -a -o bin/tcg main.go
	cd bin; tar -zcvf tcg_${VERSION}_darwin_amd64.tar.gz *; cd ..
	mv bin/tcg bin/tcg_darwin

.PHONY: show_coverage
show_coverage:
	go tool cover -html=cover.out