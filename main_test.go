package main

import (
	"log"
	"os"
	"testing"

	"github.com/matryer/is"
)

var testData = struct {
	VerifyToken struct {
		Good string
		Bad  string
	}
	VerifyPath struct {
		DirExists    string
		DirNotExists string
		FileExists   string
	}
}{
	VerifyToken: struct {
		Good string
		Bad  string
	}{
		Good: "abcdefghijklmn.atlasv1.opqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1111111111111111111",
		Bad:  "abcdefghijklmn.atlasv1.opqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!111111111111111111",
	},
	VerifyPath: struct {
		DirExists    string
		DirNotExists string
		FileExists   string
	}{
		DirExists:    "./bin",
		DirNotExists: "./tfconf",
		FileExists:   "./main.go",
	},
}

func TestGetUserHomeDirectory(t *testing.T) {
	is := is.New(t)

	homeEnv := os.Getenv("HOME")
	if homeEnv == "" {
		is.Fail()
	}

	dirStr, err := getUserHomeDirectory()
	is.NoErr(err)

	is.Equal(homeEnv, dirStr)
}

func TestVerifyToken(t *testing.T) {
	var err error
	is := is.New(t)

	err = verifyToken(testData.VerifyToken.Good)
	t.Logf("testing Good input, err: %v", err)
	is.NoErr(err)

	err = verifyToken(testData.VerifyToken.Bad)
	t.Logf("testing Bad input, err: %v", err)
	if err == nil {
		is.Fail()
	}
}

func TestVerifyPath(t *testing.T) {
	var err error
	is := is.New(t)

	_, err = verifyPath(testData.VerifyPath.DirExists)
	t.Logf("testing DirExists, err: %v", err)
	is.NoErr(err)

	_, err = verifyPath(testData.VerifyPath.DirNotExists)
	t.Logf("testing DirNotExists, err: %v", err)
	is.NoErr(err)
	defer func() {
		if err := os.RemoveAll(testData.VerifyPath.DirNotExists); err != nil {
			log.Fatal(err)
		}
	}()

	_, err = verifyPath(testData.VerifyPath.FileExists)
	t.Logf("testing FileExists, err: %v", err)
	if err == nil {
		is.Fail()
	}
}
