package main

import (
	"fmt"
	"log"
	"os"
	"os/user"
	"path"
	"regexp"

	"github.com/spf13/cobra"
)

func getUserHomeDirectory() (string, error) {
	var (
		usr     *user.User
		homeDir string
		err     error
	)

	usr, err = user.Current()
	if err != nil {
		return "", fmt.Errorf("unable to determine the current user due to error: %w", err)
	}

	homeDir = usr.HomeDir

	return homeDir, err
}

func verifyToken(token string) error {
	r := regexp.MustCompile("^([a-zA-Z0-9]{14}.atlasv1.[a-zA-Z0-9]{67})$")

	if r.MatchString(token) {
		return nil
	}

	return fmt.Errorf("the token \"%s\" does not conform to the token standards", token)
}

func verifyPath(pth string) (string, error) {
	s, err := os.Stat(pth)
	if err != nil {
		if os.IsNotExist(err) {
			if err := os.MkdirAll(pth, 0751); err != nil {
				return "", err
			}
			_, err := os.Stat(pth)
			if err != nil {
				return "", err
			}
			return path.Clean(pth), nil
		}
		return "", err
	}

	switch s.IsDir() {
	case true:
		return path.Clean(pth), nil
	default:
		return "", fmt.Errorf("%s is a regular file, not a directory", pth)
	}
}

func writeConfigFile(pth, token string) error {
	p, err := verifyPath(pth)
	if err != nil {
		return err
	}

	f, err := os.OpenFile(path.Join(p, ".terraformrc"), os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0660)
	if err != nil {
		return err
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	_, err = f.WriteString(fmt.Sprintf(configTemplate, token))
	if err != nil {
		return err
	}

	return nil
}

func execute() {
	var (
		tokenFlag     string
		setConfigPath string
		tcgCmd        = &cobra.Command{
			Use:     "tcg",
			Version: "0.2.0",
			Short:   "tcg - terraform config generator",
			Run: func(ccmd *cobra.Command, args []string) {
				switch tokenFlag {
				case "":
					log.Fatal(fmt.Errorf("the token flag may not be left blank"))
				default:
					if err := verifyToken(tokenFlag); err != nil {
						log.Fatal(err)
					}
					switch setConfigPath {
					case "":
						h, err := getUserHomeDirectory()
						if err != nil {
							log.Fatal(err)
						}
						log.Printf("setting config file to %s/.terraformrc\n", h)
						if err := writeConfigFile(h, tokenFlag); err != nil {
							log.Fatal(err)
						}
					default:
						log.Printf("setting config file to %s/.terraformrc\n", setConfigPath)
						if err := writeConfigFile(setConfigPath, tokenFlag); err != nil {
							log.Fatal(err)
						}
						os.Setenv("TF_CLI_CONFIG_FILE", setConfigPath)
					}
				}
			},
		}
	)

	tcgCmd.Flags().StringVarP(&tokenFlag, "token", "t", "", "The token generate the config with")
	tcgCmd.Flags().StringVarP(&setConfigPath, "set-config-path", "c", "", "Sets the \"TF_CLI_CONFIG_FILE\" environment variable to the value specified here and writes the config file to that location")
	tcgCmd.MarkFlagRequired("token")

	if err := tcgCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	execute()
}

const configTemplate string = `credentials "app.terraform.io" {
  token = "%s"
}`
