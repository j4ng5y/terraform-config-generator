module gitlab.com/j4ng5y/terraform-config-generator

go 1.13

require (
	github.com/matryer/is v1.2.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.5 // indirect
)
